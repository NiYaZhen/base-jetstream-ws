import { Injectable } from '@angular/core';
import { TransferInfo, NatsStreamConfig, SubscribeType } from '.';

import { Observable, Subject, from, mergeMap, tap } from 'rxjs';

import {
  Codec,
  ConsumerInfo,
  ConsumerMessages,
  JSONCodec,
  JetStreamClient,
  JetStreamManager,
  NatsConnection,
  connect
} from 'nats.ws';
export { Codec, JSONCodec, Msg } from 'nats.ws';

@Injectable({
  providedIn: 'root'
})
export class JetstreamWsService {
  replyMessage!: TransferInfo<any>;
  #streamConfig!: NatsStreamConfig;
  #natsConnection!: NatsConnection;
  #jetStreamManager!: JetStreamManager;
  #jetStreamClient!: JetStreamClient;
  #jsonCodec: Codec<any> = JSONCodec();
  #url: string = '';

  constructor(streamConfig?: NatsStreamConfig) {
    this.#streamConfig = streamConfig || { name: '', subjects: [] };
  }

  async connect(url: string) {
    try {
      this.#natsConnection = await connect({
        servers: url
      });

      this.#url = url;
    } catch (error) {
      console.error(error);
    }

    this.#jetStreamManager = await this.#natsConnection.jetstreamManager();
    this.#jetStreamClient = await this.#natsConnection.jetstream();
  }

  // 發送事件訊息
  async publish<T>(subject: string, info: TransferInfo<T>) {
    try {
      await this.#jetStreamClient.publish(
        subject,
        this.#jsonCodec.encode(info)
      );
    } catch (error) {
      console.log(error);
    }
  }

  // 接收事件訊息
  async subscribe1(name: string) {
    console.log(`into subscribe1`);
    console.log(`this.#streamConfig.name : ${this.#streamConfig.name}`);
    console.log(`name : ${name}`);

    const consumer = await this.#jetStreamClient.consumers.get(
      this.#streamConfig!.name,
      name
    );
    return await consumer.consume();
  }

  // 接收事件訊息
  subscribe(
    subject: string,
    model: SubscribeType = SubscribeType.Pull
  ): Observable<ConsumerMessages> {
    try {
      const subConsumerInfo = new Subject<Promise<ConsumerMessages>>();
      const finalInfo = new Subject<ConsumerMessages>();
      console.log(`into subscribe`);

      // Get consumer list
      const consumers = new Observable<ConsumerInfo[]>((subscriber) => {
        console.log(`into consumers`);

        this.#jetStreamManager.consumers
          .list(this.#streamConfig.name)
          .next()
          .then((result) => {
            console.log(`consumers list : ${result}}`);

            subscriber.next(result);
          });
      });
      consumers.subscribe((result) => {
        console.log(`into consumers.subscribe`);
        subConsumerInfo.next(
          model === SubscribeType.Push
            ? this.#pushSubscribe(subject)
            : this.#pullSubscribe(subject, result)
        );
      });

      subConsumerInfo
        .pipe(
          mergeMap((resultPromise) => from(resultPromise)),
          tap((result) => {
            console.log(`result : ${result}`);
          })
        )
        .subscribe({
          next: (result) => {
            console.log(`into subConsumerInfo.subscribe`);
            finalInfo.next(result);
          },
          error: (error) => {
            console.error(`An error occurred: ${error}`);
          },
          complete: () => {
            console.log(`subConsumerInfo subscription completed`);
          }
        });

      return finalInfo.asObservable();
    } catch (error) {
      console.error(error);
      return new Observable((excutor) => {});
    }
  }

  async #pushSubscribe(subject: string): Promise<ConsumerMessages> {
    return (
      await this.#jetStreamClient.consumers.get(this.#streamConfig.name, {
        filterSubjects: subject
      })
    ).consume();
  }

  async #pullSubscribe(
    subject: string,
    consumers: ConsumerInfo[]
  ): Promise<ConsumerMessages> {
    // If consumer is pull mode, filter the durable name and subject
    const consumer = consumers.find(
      (consumer) =>
        consumer.config.durable_name !== undefined &&
        consumer.config.filter_subject === subject
    );

    console.log(`consumer: ${JSON.stringify(consumer)}`);
    console.log(`streamConfig.name : ${this.#streamConfig.name}`);

    return (
      await this.#jetStreamClient.consumers.get(
        this.#streamConfig.name,
        consumer?.config.durable_name
      )
    ).consume();
  }

  async request<T>(subject: string, message: TransferInfo<T>) {
    return await from(
      this.#natsConnection.request(subject, this.#jsonCodec.encode(message))
    );
  }

  // 取消連線
  async drain() {
    await this.#natsConnection.drain();
  }
}
