import { TransferInfo, NatsStreamConfig, SubscribeType } from '.';
import { Observable } from 'rxjs';
import { ConsumerMessages } from 'nats.ws';
export { Codec, JSONCodec, Msg } from 'nats.ws';
export declare class JetstreamWsService {
    #private;
    replyMessage: TransferInfo<any>;
    constructor(streamConfig?: NatsStreamConfig);
    connect(url: string): Promise<void>;
    publish<T>(subject: string, info: TransferInfo<T>): Promise<void>;
    subscribe1(name: string): Promise<ConsumerMessages>;
    subscribe(subject: string, model?: SubscribeType): Observable<ConsumerMessages>;
    request<T>(subject: string, message: TransferInfo<T>): Promise<Observable<import("nats.ws").Msg>>;
    drain(): Promise<void>;
}
