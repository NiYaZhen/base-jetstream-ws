import { ConnectionOptions, StreamConfig } from 'nats.ws';
export interface TransferInfo<T> {
    data: T;
}
export interface NatsConnectionOptions extends ConnectionOptions {
    name: string;
}
export interface NatsStreamConfig extends Partial<StreamConfig> {
    name: string;
    subjects: string[];
}
export declare enum SubscribeType {
    Pull = "pull",
    Push = "push"
}
export declare enum AckPolicy {
    /**
     * 不確認任何消息
     */
    None = "none",
    /**
     * 確認收到的最後一個消息，之前收到的所有消息將在同一時間自動確認
     */
    All = "all",
    /**
     * 每個個別消息都必須被確認
     */
    Explicit = "explicit",
    /**
     * @ignore
     */
    NotSet = ""
}
export declare enum RetentionPolicy {
    /**
     * 保留訊息直到達到限制，然後觸發丟棄策略。
     */
    Limits = "limits",
    /**
     * 在特定主題上存在消費者興趣時保留訊息。
     */
    Interest = "interest",
    /**
     * 保留訊息直到被確認。
     */
    Workqueue = "workqueue"
}
export declare enum DeliverPolicy {
    /**
     * Deliver all messages
     */
    All = "all",
    /**
     * Deliver starting with the last message
     */
    Last = "last",
    /**
     * Deliver starting with new messages
     */
    New = "new",
    /**
     * Deliver starting with the specified sequence
     */
    StartSequence = "by_start_sequence",
    /**
     * Deliver starting with the specified time
     */
    StartTime = "by_start_time",
    /**
     * Deliver starting with the last messages for every subject
     */
    LastPerSubject = "last_per_subject"
}
