import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { SubscribeType } from '.';
import { Observable, Subject, from, mergeMap, tap } from 'rxjs';
import { JSONCodec, connect } from 'nats.ws';
export { JSONCodec } from 'nats.ws';
export let JetstreamWsService = class JetstreamWsService {
    #streamConfig;
    #natsConnection;
    #jetStreamManager;
    #jetStreamClient;
    #jsonCodec = JSONCodec();
    #url = '';
    constructor(streamConfig) {
        this.#streamConfig = streamConfig || { name: '', subjects: [] };
    }
    async connect(url) {
        try {
            this.#natsConnection = await connect({
                servers: url
            });
            this.#url = url;
        }
        catch (error) {
            console.error(error);
        }
        this.#jetStreamManager = await this.#natsConnection.jetstreamManager();
        this.#jetStreamClient = await this.#natsConnection.jetstream();
    }
    // 發送事件訊息
    async publish(subject, info) {
        try {
            await this.#jetStreamClient.publish(subject, this.#jsonCodec.encode(info));
        }
        catch (error) {
            console.log(error);
        }
    }
    // 接收事件訊息
    async subscribe1(name) {
        console.log(`into subscribe1`);
        console.log(`this.#streamConfig.name : ${this.#streamConfig.name}`);
        console.log(`name : ${name}`);
        const consumer = await this.#jetStreamClient.consumers.get(this.#streamConfig.name, name);
        return await consumer.consume();
    }
    // 接收事件訊息
    subscribe(subject, model = SubscribeType.Pull) {
        try {
            const subConsumerInfo = new Subject();
            const finalInfo = new Subject();
            console.log(`into subscribe`);
            // Get consumer list
            const consumers = new Observable((subscriber) => {
                console.log(`into consumers`);
                this.#jetStreamManager.consumers
                    .list(this.#streamConfig.name)
                    .next()
                    .then((result) => {
                    console.log(`consumers list : ${result}}`);
                    subscriber.next(result);
                });
            });
            consumers.subscribe((result) => {
                console.log(`into consumers.subscribe`);
                subConsumerInfo.next(model === SubscribeType.Push
                    ? this.#pushSubscribe(subject)
                    : this.#pullSubscribe(subject, result));
            });
            subConsumerInfo
                .pipe(mergeMap((resultPromise) => from(resultPromise)), tap((result) => {
                console.log(`result : ${result}`);
            }))
                .subscribe({
                next: (result) => {
                    console.log(`into subConsumerInfo.subscribe`);
                    finalInfo.next(result);
                },
                error: (error) => {
                    console.error(`An error occurred: ${error}`);
                },
                complete: () => {
                    console.log(`subConsumerInfo subscription completed`);
                }
            });
            return finalInfo.asObservable();
        }
        catch (error) {
            console.error(error);
            return new Observable((excutor) => { });
        }
    }
    async #pushSubscribe(subject) {
        return (await this.#jetStreamClient.consumers.get(this.#streamConfig.name, {
            filterSubjects: subject
        })).consume();
    }
    async #pullSubscribe(subject, consumers) {
        // If consumer is pull mode, filter the durable name and subject
        const consumer = consumers.find((consumer) => consumer.config.durable_name !== undefined &&
            consumer.config.filter_subject === subject);
        console.log(`consumer: ${JSON.stringify(consumer)}`);
        console.log(`streamConfig.name : ${this.#streamConfig.name}`);
        return (await this.#jetStreamClient.consumers.get(this.#streamConfig.name, consumer?.config.durable_name)).consume();
    }
    async request(subject, message) {
        return await from(this.#natsConnection.request(subject, this.#jsonCodec.encode(message)));
    }
    // 取消連線
    async drain() {
        await this.#natsConnection.drain();
    }
};
JetstreamWsService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], JetstreamWsService);
//# sourceMappingURL=jetstream-ws.service.js.map